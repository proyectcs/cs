-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
-- dsdsdsdssss
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-12-2017 a las 07:23:28
-- Versión del servidor: 10.1.19-MariaDB
-- Versión de PHP: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `colegio`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumna`
--

CREATE TABLE `alumna` (
  `Alum_DNI` int(8) NOT NULL,
  `Alum_Nombre` text NOT NULL,
  `Alum_Apellido` text NOT NULL,
  `Alum_Nacimiento` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `archivo_notas`
--

CREATE TABLE `archivo_notas` (
  `Arch_Nota_ID` int(11) NOT NULL,
  `Arch_Nota_Fecha` date NOT NULL,
  `Arch_Nota_Profesor` int(8) NOT NULL,
  `Arch_Nota_Achivo` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asistencia`
--

CREATE TABLE `asistencia` (
  `Asis_ID` int(10) NOT NULL,
  `Asis_Alumna` int(8) NOT NULL,
  `Asis_Fecha` date NOT NULL,
  `Asis_Condicion` text NOT NULL,
  `Asis_Presentacion` text,
  `Asis_Observacion` mediumtext,
  `Asis_Auxiliar` int(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `autorizacion`
--

CREATE TABLE `autorizacion` (
  `Auto_ID` int(11) NOT NULL,
  `Auto_Profesor` int(11) NOT NULL,
  `Auto_Alumna` int(11) NOT NULL,
  `Auto_Curso` int(2) NOT NULL,
  `Auto_Fecha` date NOT NULL,
  `Auto_Motivo` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auxiliar`
--

CREATE TABLE `auxiliar` (
  `Auxi_DNI` int(8) NOT NULL,
  `Auxi_Nombre` varchar(50) NOT NULL,
  `Auxi_Apellido` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `citacion`
--

CREATE TABLE `citacion` (
  `Cita_ID` int(11) NOT NULL,
  `Cita_Fecha` date NOT NULL,
  `Cita_Hora` time DEFAULT NULL,
  `Cita_Apoderado` int(8) NOT NULL,
  `Cita_Alumna` int(8) NOT NULL,
  `Cita_Motivo` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `curso`
--

CREATE TABLE `curso` (
  `Curs_ID` int(2) NOT NULL,
  `Curs_Nombre` char(20) NOT NULL,
  `Prof_DNI` int(11) NOT NULL,
  `Curs_Anual` int(4) NOT NULL,
  `Curs_Seccion` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `curso`
--

INSERT INTO `curso` (`Curs_ID`, `Curs_Nombre`, `Prof_DNI`, `Curs_Anual`, `Curs_Seccion`) VALUES
(1, 'Matematica', 12345678, 2017, 'A'),
(2, 'comuni', 12345678, 2017, 'B'),
(3, 'comunicaciones', 12345678, 2017, 'C');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `justificacion`
--

CREATE TABLE `justificacion` (
  `Just_ID` int(11) NOT NULL,
  `Just_Fecha` date NOT NULL,
  `Just_Alumna` int(11) NOT NULL,
  `Just_Apoderado` int(11) NOT NULL,
  `Just_Motivo` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notas`
--

CREATE TABLE `notas` (
  `Nota_ID` int(11) NOT NULL,
  `Nota_Unidad` int(2) NOT NULL,
  `Nota_Promedio` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `padre`
--

CREATE TABLE `padre` (
  `Padr_DNI` int(8) NOT NULL,
  `Padr_Nombre` text NOT NULL,
  `Padre_Apellido` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `padre_alumna`
--

CREATE TABLE `padre_alumna` (
  `Padr_Alum_ID` int(11) NOT NULL,
  `Padr_Alum_Padre` int(8) NOT NULL,
  `Padr_Alum_Alumna` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parte`
--

CREATE TABLE `parte` (
  `Part_ID` int(11) NOT NULL,
  `Part_Fecha` date NOT NULL,
  `Part_Grado` int(1) NOT NULL,
  `Part_Seccion` char(1) NOT NULL,
  `Part_Curso` char(20) NOT NULL,
  `Part_Porcentaje` char(20) NOT NULL,
  `Part_Observaciones` text,
  `Part_Codigo` char(20) NOT NULL,
  `Prof_DNI` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesor`
--

CREATE TABLE `profesor` (
  `Prof_DNI` int(11) NOT NULL,
  `Prof_Nombre` char(20) NOT NULL,
  `Prof_Apellido` char(20) NOT NULL,
  `Prof_Asistencia` char(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `profesor`
--

INSERT INTO `profesor` (`Prof_DNI`, `Prof_Nombre`, `Prof_Apellido`, `Prof_Asistencia`) VALUES
(12345678, 'Estanislao', 'Sanchez', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `programacion`
--

CREATE TABLE `programacion` (
  `Prog_ID` int(11) NOT NULL,
  `Prog_Tipo` int(1) NOT NULL COMMENT 'Programacion anual, semestral, bimestral (1,2,3)',
  `Prog_Plantilla` blob,
  `Prog_PlantillaUp` blob NOT NULL,
  `Prof_DNI` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tutor`
--

CREATE TABLE `tutor` (
  `Tuto_Seccion` char(1) NOT NULL,
  `Tuto_Grado` int(1) NOT NULL,
  `Prof_DNI` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alumna`
--
ALTER TABLE `alumna`
  ADD PRIMARY KEY (`Alum_DNI`);

--
-- Indices de la tabla `archivo_notas`
--
ALTER TABLE `archivo_notas`
  ADD PRIMARY KEY (`Arch_Nota_ID`),
  ADD KEY `Arch_Nota_Profesor` (`Arch_Nota_Profesor`);

--
-- Indices de la tabla `asistencia`
--
ALTER TABLE `asistencia`
  ADD PRIMARY KEY (`Asis_ID`),
  ADD KEY `Asis_Auxiliar` (`Asis_Auxiliar`);

--
-- Indices de la tabla `autorizacion`
--
ALTER TABLE `autorizacion`
  ADD PRIMARY KEY (`Auto_ID`),
  ADD KEY `Auto_Docente` (`Auto_Profesor`,`Auto_Alumna`,`Auto_Curso`),
  ADD KEY `Auto_Curso` (`Auto_Curso`),
  ADD KEY `Auto_Alumna` (`Auto_Alumna`);

--
-- Indices de la tabla `auxiliar`
--
ALTER TABLE `auxiliar`
  ADD PRIMARY KEY (`Auxi_DNI`);

--
-- Indices de la tabla `citacion`
--
ALTER TABLE `citacion`
  ADD PRIMARY KEY (`Cita_ID`),
  ADD KEY `Cita_Apoderado` (`Cita_Apoderado`,`Cita_Alumna`),
  ADD KEY `Cita_Alumna` (`Cita_Alumna`);

--
-- Indices de la tabla `curso`
--
ALTER TABLE `curso`
  ADD PRIMARY KEY (`Curs_ID`,`Prof_DNI`),
  ADD KEY `Relationship7` (`Prof_DNI`);

--
-- Indices de la tabla `justificacion`
--
ALTER TABLE `justificacion`
  ADD PRIMARY KEY (`Just_ID`),
  ADD KEY `Just_Alumno` (`Just_Alumna`,`Just_Apoderado`);

--
-- Indices de la tabla `notas`
--
ALTER TABLE `notas`
  ADD PRIMARY KEY (`Nota_ID`);

--
-- Indices de la tabla `padre`
--
ALTER TABLE `padre`
  ADD PRIMARY KEY (`Padr_DNI`);

--
-- Indices de la tabla `padre_alumna`
--
ALTER TABLE `padre_alumna`
  ADD PRIMARY KEY (`Padr_Alum_ID`),
  ADD KEY `Padr_Alum_Padre` (`Padr_Alum_Padre`,`Padr_Alum_Alumna`),
  ADD KEY `Padr_Alum_Alumna` (`Padr_Alum_Alumna`);

--
-- Indices de la tabla `parte`
--
ALTER TABLE `parte`
  ADD PRIMARY KEY (`Prof_DNI`,`Part_ID`);

--
-- Indices de la tabla `profesor`
--
ALTER TABLE `profesor`
  ADD PRIMARY KEY (`Prof_DNI`);

--
-- Indices de la tabla `programacion`
--
ALTER TABLE `programacion`
  ADD PRIMARY KEY (`Prog_ID`,`Prof_DNI`),
  ADD KEY `Relationship15` (`Prof_DNI`);

--
-- Indices de la tabla `tutor`
--
ALTER TABLE `tutor`
  ADD PRIMARY KEY (`Prof_DNI`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `archivo_notas`
--
ALTER TABLE `archivo_notas`
  MODIFY `Arch_Nota_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `asistencia`
--
ALTER TABLE `asistencia`
  MODIFY `Asis_ID` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `autorizacion`
--
ALTER TABLE `autorizacion`
  MODIFY `Auto_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `citacion`
--
ALTER TABLE `citacion`
  MODIFY `Cita_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `justificacion`
--
ALTER TABLE `justificacion`
  MODIFY `Just_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `notas`
--
ALTER TABLE `notas`
  MODIFY `Nota_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `padre_alumna`
--
ALTER TABLE `padre_alumna`
  MODIFY `Padr_Alum_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `programacion`
--
ALTER TABLE `programacion`
  MODIFY `Prog_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `archivo_notas`
--
ALTER TABLE `archivo_notas`
  ADD CONSTRAINT `archivo_notas_ibfk_1` FOREIGN KEY (`Arch_Nota_Profesor`) REFERENCES `profesor` (`Prof_DNI`);

--
-- Filtros para la tabla `asistencia`
--
ALTER TABLE `asistencia`
  ADD CONSTRAINT `asistencia_ibfk_1` FOREIGN KEY (`Asis_Auxiliar`) REFERENCES `auxiliar` (`Auxi_DNI`);

--
-- Filtros para la tabla `autorizacion`
--
ALTER TABLE `autorizacion`
  ADD CONSTRAINT `autorizacion_ibfk_3` FOREIGN KEY (`Auto_Curso`) REFERENCES `curso` (`Curs_ID`),
  ADD CONSTRAINT `autorizacion_ibfk_4` FOREIGN KEY (`Auto_Alumna`) REFERENCES `alumna` (`Alum_DNI`),
  ADD CONSTRAINT `autorizacion_ibfk_5` FOREIGN KEY (`Auto_Profesor`) REFERENCES `profesor` (`Prof_DNI`);

--
-- Filtros para la tabla `citacion`
--
ALTER TABLE `citacion`
  ADD CONSTRAINT `citacion_ibfk_1` FOREIGN KEY (`Cita_Alumna`) REFERENCES `alumna` (`Alum_DNI`);

--
-- Filtros para la tabla `curso`
--
ALTER TABLE `curso`
  ADD CONSTRAINT `Relationship7` FOREIGN KEY (`Prof_DNI`) REFERENCES `profesor` (`Prof_DNI`);

--
-- Filtros para la tabla `justificacion`
--
ALTER TABLE `justificacion`
  ADD CONSTRAINT `justificacion_ibfk_1` FOREIGN KEY (`Just_Alumna`) REFERENCES `alumna` (`Alum_DNI`);

--
-- Filtros para la tabla `padre_alumna`
--
ALTER TABLE `padre_alumna`
  ADD CONSTRAINT `padre_alumna_ibfk_1` FOREIGN KEY (`Padr_Alum_Padre`) REFERENCES `padre` (`Padr_DNI`),
  ADD CONSTRAINT `padre_alumna_ibfk_2` FOREIGN KEY (`Padr_Alum_Alumna`) REFERENCES `alumna` (`Alum_DNI`);

--
-- Filtros para la tabla `parte`
--
ALTER TABLE `parte`
  ADD CONSTRAINT `Relationship9` FOREIGN KEY (`Prof_DNI`) REFERENCES `profesor` (`Prof_DNI`);

--
-- Filtros para la tabla `programacion`
--
ALTER TABLE `programacion`
  ADD CONSTRAINT `Relationship15` FOREIGN KEY (`Prof_DNI`) REFERENCES `profesor` (`Prof_DNI`);

--
-- Filtros para la tabla `tutor`
--
ALTER TABLE `tutor`
  ADD CONSTRAINT `Relationship10` FOREIGN KEY (`Prof_DNI`) REFERENCES `profesor` (`Prof_DNI`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
