#README
DETALLES
Titulo:
	Plataforma de Control Estudiantil
Pre-requisitos: 
	* MySQL
	* PHP
	* Java
	* HTML
	* CSS
	* JavaScript
Instalación:
	* Plataforma Web
	
	```
	psqldump import database.sql
	```
Caracteristicas:
	> El colegio nos ha proporcionado la DB de Alumno, cada alumna tiene 100 campos, implementada usando PHP y MySql.
	1. Alumno: 
		* Recibe datos ingresados por el Usuario
	2. Psicologia:
		* Login 
		* Busqueda por codigo de alumno
		* Visualizar las citas por alumno
		* Visualizar el numero de citas por alumno
		* Agregar cita
	3. Enfermeria:
		* Login
		* Busqueda por codigo de alumno
		* Busqueda de alumnos por Apellidos
		* Llenar Historial Clinico
		* Llenar ficha de Compromiso
		* Llenar ficha de Seguimiento
	4. Auxiliar:
		* Login
		* Registro de Asistencia de las Alumnas
		* Registro de Presentacion Personal de las Alumnas
		* Recibir notificaciones hechas por Tutores o Docentes
		* Realizar justificacion
		* Realizar autorizacion 
	5. Profesor:
		* Login 
		* Control de Asistencia por Curso
		* Parte
		* Subir Programacion
		* Subir Notas
	6. Administrador:
		* Registro Profesor
		* Modificar datos de Profesor
		* Eliminar datos de Profesor
		* Registro Auxiliar
		* Modificar datos de Auxiliar
		* Eliminar datos de Auxiliar
		* Registro Psicologo
		* Modificar datos de Psicologo
		* Eliminar datos de Psicologo
		* Registro Enfermera
		* Modificar datos de Enfermera
		* Eliminar datos de Enfermera